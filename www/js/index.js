/**
 * Created by kirill.vladykin on 06.07.2016.
 */

const sidebarWidth = 300;
const sidebarOpenArea = 50;
const sidebarOpenMinDistance = 10;

//var menu = new Sidebar(false);
var timer;

function init() {
    // logger init
    document.querySelector("#log").style.display = "block";

    // global shadow init
    document.querySelector("#global-shadow").style.display = "none";

    /*// sidebar dimensions init
    var sidebarWidth = parseInt(document.querySelector("#sidebar").style.width);
    if (sidebarWidth > screen.width*0.9) {
        document.querySelector("#sidebar").style.width = screen.width * 0.9 + "px";
    }
    document.querySelector("#sidebar").style.height = screen.height + "px";

    // sidebar events init
    document.querySelector("#sidebar-open").addEventListener("click", function() { menu.open(); }, false);
    document.querySelector("#sidebar-gohome").addEventListener("click", function() { menu.close(); }, false);
    */

    /*var body = document.querySelector("body");

    body.addEventListener("touchstart", function(evnt) {
        var touches = evnt.changedTouches;
        for (var i=0; i < touches.length; i++) {
            log("touchstart[" + i + "] " + touches[i].pageX + " " + touches[i].pageY);
        }
        menu.handleTouch(evnt.changedTouches[0].pageX, evnt.changedTouches[0].pageY);
    }, false);

    body.addEventListener("touchend", function(evnt) {
        var touches = evnt.changedTouches;
        for (var i=0; i < touches.length; i++) {
            log("touchend[" + i + "] " + touches[i].pageX + " " + touches[i].pageY);
        }
        menu.handleTouch(evnt.changedTouches[0].pageX, evnt.changedTouches[0].pageY);
    }, false);*/

    /*timer = new SmokeTimer({
        tickIntervalInSeconds: 1,
        container: document.querySelector("#timer")
    });
    timer.setTimeComponents(1, 30);
    timer.run();*/
}

function log(data) {
    var li = document.createElement("li");
    var node = document.createTextNode(data);
    li.appendChild(node);
    var log = $("#log");
    log.prepend(li);
}


