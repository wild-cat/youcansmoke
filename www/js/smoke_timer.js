/**
 * Created by Кирилл on 13.07.2016.
 */

/**
 * @param {Object} options
 * @constructor
 */
function SmokeTimer(options) {
    if (options.tickIntervalInSeconds !== 1 && options.tickIntervalInSeconds !== 0.1) {
        console.error(new Error("Allowed values for 'tickIntervalInSeconds' are 1 and 0.1, not " + options.tickIntervalInSeconds));
        return;
    }

    Timer.apply(this, arguments);

    if (!options.container) {
        console.error(new Error("Constructor must be provided with a DOM container"));
        this._state = SmokeTimer.prototype.NOT_INITIALIZED;
        return;
    }
    this._container = options.container;
    this._createLayout();
    this._isShowingMs = options.showMs || false;
    this._isShowingHours = options.showHours || true;
}

SmokeTimer.prototype = Object.create(Timer.prototype);
SmokeTimer.prototype.constructor = SmokeTimer;
SmokeTimer.prototype.NOT_INITIALIZED = undefined;

/**
 * @param fn(ticksLeft: int) tickCallback
 * @param fn(err: Error|null) finishCallback
 */
Timer.prototype.run = function(tickCallback, finishCallback) {
    if (this._state !== Timer.prototype.IDLE) {
        console.log("Warn: cannot run timer because it is not idle");
        return;
    }

    var that = this;
    this._state = Timer.prototype.RUNNING;

    that._calculateTimeComponents();
    that._drawFormattedTime();

    this.pointerToInterval = setInterval(function() {
        that._ticksLeft--;
        that._calculateTimeComponents();
        that._drawFormattedTime();
        if (that._ticksLeft === 0) {
            // Let's stop interval here, for sure
            clearInterval(that.pointerToInterval);
            that.stop(finishCallback);
        } else if (tickCallback) {
            tickCallback(that._ticksLeft);
        }
    }, this._tickIntervalInSeconds * 1000);
};

SmokeTimer.prototype.setTimeComponents = function(hours, minutes, seconds, ms) {
    if (this._state !== Timer.prototype.IDLE) {
        console.log("Warn: Cannot set timer's interval because it's state is not IDLE");
        return;
    }
    if (!hours && !minutes && !seconds && !ms) {
        console.log("Warn: all values are undefined or null");
        return;
    }
    if (hours === 0 && minutes === 0 && seconds === 0 && ms === 0) {
        console.log("Warn: Cannot set all zero's values");
        return;
    }
    if (hours && (hours < 0 || hours > 23)) {
        console.log("Warn: given hours value is incorrect");
        return;
    }
    if (minutes && (minutes < 0 || minutes > 59)) {
        console.log("Warn: given minutes value is incorrect");
        return;
    }
    if (seconds && (seconds < 0 || seconds > 59)) {
        console.log("Warn: given seconds value is incorrect");
        return;
    }
    if (ms && (ms < 0 || ms > 9)) {
        console.log("Warn: given seconds value is incorrect");
        return;
    }
    
    if (hours && !minutes && !seconds && !ms) {
        this.setIntervalInSeconds(hours * 3600);
    } else if (minutes && !seconds && !ms) {
        this.setIntervalInSeconds(hours * 3600 + minutes * 60);
    } else if (seconds && !ms) {
        this.setIntervalInSeconds(hours * 3600 + minutes * 60 + seconds);
    } else if (ms) {
        this.setIntervalInSeconds(minutes * 60 + seconds + ms * 0.1);
    } else {
        console.error("Warn: bad parameters: ", hours, minutes, seconds, ms);
        return;
    }

    this._ticksLeft = this._intervalInSeconds / this._tickIntervalInSeconds;
};

SmokeTimer.prototype._createLayout = function() {
    this._container.className = "smoke-timer";
    this._container.innerHTML = "00:00:0";
    var label = document.createElement("span");
    label.className = "label";
    this._container.appendChild(label);
};

SmokeTimer.prototype._drawFormattedTime = function() {
    var mm = (this.mm < 10) ? "0" + this.mm : this.mm;
    var ss = (this.ss < 10) ? "0" + this.ss : this.ss;
    var hh = "";
    var ms = "";
    if (this._isShowingHours) {
        hh = this.hh + ":";
    }
    if (this._isShowingMs) {
        ms = ":" + this.ms;
    }
    this._container.innerHTML = hh + mm + ":" + ss + ms;
};

SmokeTimer.prototype._calculateTimeComponents = function() {
    if (this._tickIntervalInSeconds === 1) {
        // 1 tick === 1 second
        this.hh = Math.floor(this._ticksLeft / 3600);
        var x = this._ticksLeft % 3600;
        this.mm = Math.floor(x / 60);
        this.ss = this._ticksLeft % 60;
        this.ms = 0;
    } else if (this._tickIntervalInSeconds === 0.1) {
        // 1 tick === 100 ms
        this.hh = Math.floor(this._ticksLeft / 36000);
        x = this._ticksLeft % 36000;
        this.mm = Math.floor(x / 600);
        var y = this._ticksLeft % 600;
        this.ss = Math.floor(y / 10);
        this.ms =  x % 10;
    }

};



