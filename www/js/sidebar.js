function Sidebar(isOpen) {
    this._isOpen = isOpen || false;
    this._isFirstTouch = true;
    this._firstTouchX = 0;
}

Sidebar.prototype.open = function(callback) {
    this._move("open", callback);
};

Sidebar.prototype.close = function(callback) {
    this._move("close", callback);
};

Sidebar.prototype._move = function(direction, callback) {
    if ((direction === "open" && this._isOpen) || (direction === "close" && !this._isOpen)) {
        if (callback) {
            if (direction === "open") {
                callback(new Error("Sidebar is already opened"));
            } else {
                callback(new Error("Sidebar is already closed"));
            }
        } else {
            return;
        }
    } else {

        //console.log("animated " + direction);
        if (direction === "open") {
            document.querySelector("#sidebar").className = "sidebar-out"; //run the animation
            document.querySelector("#global-shadow").style.display = "block";
        } else {
            document.querySelector("#sidebar").className = "sidebar-in";
            document.querySelector("#global-shadow").style.display = "none";
        }

        this._isOpen = (direction === "open");
        if (callback) {
            callback(null);
        }
    }
};

Sidebar.prototype.handleTouch = function(x, y) {
    var touchAreaWidth = (this._isOpen) ? sidebarWidth : sidebarOpenArea;
    if (this._isFirstTouch) {
        if (x > 0 && x < touchAreaWidth) {
            this._isFirstTouch = false;
            this._firstTouchX = x;
        }
    } else {
        if (x - this._firstTouchX >= sidebarOpenMinDistance) {
            this._move("open");
        } else if (this._firstTouchX > x) {
            this._move("close");
        }
        this._isFirstTouch = true;
    }
};

