/**
 * Created by Кирилл on 13.07.2016.
 */

/**
 * @param {Object} options
 * @constructor
 */
function Timer(options) {
    if (options) {
        if (options.intervalInSeconds && typeof options.intervalInSeconds !== "number") {
            console.error(new Error("options.intervalInSeconds must be of type 'number'!"));
            return;
        }
        if (options.tickIntervalInSeconds && typeof options.tickIntervalInSeconds !== "number") {
            console.error(new Error("options.tickIntervalInSeconds must be of type 'number'!"));
            return;
        }
        if (options.tickIntervalInSeconds && (options.tickIntervalInSeconds < 0.1 || options.tickIntervalInSeconds > 60)) {
            console.error(new Error("tickIntervalInSeconds must be between 0.1 & 60"));
            return;
        }
        if (options.intervalInSeconds && options.tickIntervalInSeconds) {
            var tick2 = options.tickIntervalInSeconds;
            var interval2 = options.intervalInSeconds;
            if (tick2 < 1) {
                tick2 = tick2 * 10;
                interval2 = interval2 * 10;
            }
            if (interval2 % tick2 !== 0) {
                console.error(new Error("Bad intervals!"));
                return;
            }
        }
    }
    this._options = {};
    this._defaults = {
        intervalInSeconds: 1,
        tickIntervalInSeconds: 1
    };
    Object.assign(this._options, this._defaults, options);
    this.pointerToInterval;
    this._ticksLeft;
    this._init();
}

/* Timer states */
Timer.prototype.IDLE = "0";
Timer.prototype.RUNNING = "1";

/**
 * @param {int} intervalInSeconds
 */
Timer.prototype.setIntervalInSeconds = function(intervalInSeconds) {
    if (intervalInSeconds <= 0) {
        throw new Error("Value of interval must be bigger than zero");
    }
    this._intervalInSeconds = intervalInSeconds;
};

/**
 * @returns {string|*}
 */
Timer.prototype.getState = function() {
    return this._state;
};

/**
 * @param fn(ticksLeft: int) tickCallback
 * @param fn(err: Error|null) finishCallback
 */
Timer.prototype.run = function(tickCallback, finishCallback) {
    if (this._state !== Timer.prototype.IDLE) {
        console.log("Warn: cannot run timer because it is not idle");
        return;
    }

    var that = this;
    this._state = Timer.prototype.RUNNING;

    this.pointerToInterval = setInterval(function() {
        that._ticksLeft--;
        if (that._ticksLeft === 0) {
            // Let's stop interval here, for sure
            clearInterval(that.pointerToInterval);
            that.stop(finishCallback);
        } else if (tickCallback) {
            tickCallback(that._ticksLeft);
        }
    }, this._tickIntervalInSeconds * 1000);
};

/**
 * @param fn(err:Error|null) callback
 */
Timer.prototype.stop = function(callback) {
    if (this._state !== Timer.prototype.RUNNING) {
        console.log("Warn: cannot stop timer because it is not running");
        return;
    }

    this._init();
    if (callback) {
        callback(null);
    }
};

Timer.prototype._init = function() {
    clearInterval(this.pointerToInterval);
    try {
        this.setIntervalInSeconds(this._options.intervalInSeconds);
    } catch (err) {
        console.error(err);
        return;
    }
    this._tickIntervalInSeconds = this._options.tickIntervalInSeconds;
    this._ticksLeft = this._intervalInSeconds / this._tickIntervalInSeconds;
    this._state = Timer.prototype.IDLE;
};